# IV. Operating Systems Requirements (Track-Certified Apprentice)
## Track Certification Description

*TODO*

|Task| Sub-task & Description | General Track Certification Proficiency Code |
|----|------------------------|----------------------------------------------|
| | *TODO* |

------------------

Track certification does not improve, directly, any of the core standards for the [Apprentice-3](01_requirements_apprentice3.md) level.

