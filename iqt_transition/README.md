# iqt will be transitioning to idf.

## idf will consist of 30 lectures covering topics such as c programming, python, algorithms, networking, and some x86 assembly. this will be taught over the span of about 15 weeks at plan.

### below is a brief of the course:



===== week 00 ======

1. introduction to c programming

2. if-else statements

3. loops


===== week 01 =====

4. introduction to x86 assembly (asm)

5. introduction to debugging


===== week 02 =====

6. arrays and structures

7. c-style strings

8. c basics puzzle capstone


===== week 03 =====

9. c file i/o

10. c networking


===== week 04 =====

11. pointers and function pointers

12. stacks (pragmatic) and linked lists

13. stacks and heaps

14. trees


===== week 05 =====

15. error handling

16. algorithms w/ data structures


===== week 06 =====

17. algorithsm w/ data structures (2)


===== week 07 =====

18. application of algorithms


===== week 08 =====

19. algorithm crucible


===== week 09 =====

20. alogrithm crucible review

21. python crash course


===== week 10 =====

22. c standard library calls

23. refactoring legacy code

24. code quality best practices


===== week 11 =====

25. applications of algorithms (refresher)

26. algroithm crucible (2)


===== week 12 =====

27. capstone sprint (1)


===== week 13 =====

28. capstone sprint (2)


===== week 14 =====

29. capstone sprint (3)


===== week 15 =====

30. capstone sprint (4)
