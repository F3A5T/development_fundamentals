# Intro to x86/64 Assembly and Debugging

![image](imgs/2fc.png)
### REQUIRMENTS
- C/C++ Programming Knowledge
- Linux Distro (Fedora is used for this course)
- Windows 10
- CMAKE
- GDB
- NASM
- GCC/G++
- WinDBG
- Text Editor (Visual Studio Code, VIM/Vi, Atom, Sublime Text, etc)

### Additional Info

* No current written training on debugging. 
* All debugging training is currently live demonstration, instruction and exercise
